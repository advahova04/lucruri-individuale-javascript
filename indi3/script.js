// Массив транзакций
let transactions = [];

// Функция добавления транзакции
function addTransaction(event) {
    event.preventDefault(); // Отменяем действие по умолчанию (отправку формы)
    const form = event.target; // Получаем форму, из которой было вызвано событие
    const date = form.elements.date.value; // Получаем значение поля "Дата и время" из формы
    const amount = parseFloat(form.elements.amount.value); // Получаем значение поля "Сумма" из формы, преобразовываем в число
    const category = form.elements.category.value; // Получаем значение поля "Категория" из формы
    const description = form.elements.description.value; // Получаем значение поля "Описание" из формы

    // Создаем объект транзакции
    const transaction = {
        id: generateId(), // Генерируем уникальный идентификатор
        date: date,
        amount: amount,
        category: category,
        description: description
    };

    transactions.push(transaction); // Добавляем транзакцию в массив
    renderTransaction(transaction); // Отображаем транзакцию в таблице
    calculateTotal(); // Пересчитываем общую сумму транзакций
    form.reset(); // Сбрасываем значения формы
}

// Функция удаления транзакции
function deleteTransaction(event) {
    if (event.target.classList.contains('deleteButton')) { // Проверяем, была ли нажата кнопка удаления
        const row = event.target.closest('tr'); // Находим ближайшую строку таблицы
        const id = row.dataset.id; // Получаем идентификатор транзакции из атрибута data-id
        transactions = transactions.filter(transaction => transaction.id !== id); // Удаляем транзакцию из массива
        row.remove(); // Удаляем строку таблицы
        calculateTotal(); // Пересчитываем общую сумму транзакций после удаления
    }
}

// Функция отображения транзакции в таблице
function renderTransaction(transaction) {
    const tableBody = document.getElementById('transactionTableBody'); // Получаем тело таблицы
    const row = document.createElement('tr'); // Создаем новую строку таблицы
    row.dataset.id = transaction.id; // Устанавливаем идентификатор транзакции в атрибут data-id строки

    // Получаем первые 4 слова из описания транзакции
    const shortDescription = transaction.description.split(' ').slice(0, 4).join(' ');

    // Заполняем ячейки строки данными о транзакции
    row.innerHTML = `
        <td>${transaction.id}</td>
        <td>${transaction.date}</td>
        <td>${transaction.category}</td>
        <td>${shortDescription}</td>
        <td><button class="deleteButton">Удалить</button></td>
    `;

    // Определяем цвет строки в зависимости от знака суммы транзакции
    if (transaction.amount > 0) {
        row.style.backgroundColor = '#4CAF50'; // Зеленый цвет для положительных сумм
    } else {
        row.style.backgroundColor = '#FF4933'; // Красный цвет для отрицательных сумм
    }

    // Добавляем обработчик события для отображения подробной информации о транзакции при клике на строку
    row.addEventListener('click', () => displayTransactionDetails(transaction));

    tableBody.appendChild(row); // Добавляем строку в таблицу
}

// Функция отображения подробной информации о транзакции
function displayTransactionDetails(transaction) {
    const detailsDiv = document.getElementById('transactionDetails'); // Получаем элемент, в котором будем отображать подробности
    detailsDiv.innerHTML = `
        <h2>Подробное описание транзакции</h2>
        <p><strong>ID:</strong> ${transaction.id}</p>
        <p><strong>Дата и время:</strong> ${transaction.date}</p>
        <p><strong>Сумма:</strong> ${transaction.amount}</p>
        <p><strong>Категория:</strong> ${transaction.category}</p>
        <p><strong>Описание:</strong> ${transaction.description}</p>
    `;
}

// Функция для генерации уникального ID транзакции
function generateId() {
    return Math.floor(Math.random() * 100); // Генерируем случайное число от 0 до 99
}

// Функция для подсчета общей суммы транзакций
function calculateTotal() {
    const totalAmountElement = document.getElementById('totalAmount'); // Получаем элемент, в котором будем отображать общую сумму
    const totalAmount = transactions.reduce((total, transaction) => total + transaction.amount, 0); // Вычисляем общую сумму транзакций
    totalAmountElement.textContent = totalAmount; // Отображаем общую сумму на странице
}

// Установка обработчиков событий
document.getElementById('transactionForm').addEventListener('submit', addTransaction); // Добавляем обработчик события на отправку формы
document.getElementById('transactionTable').addEventListener('click', deleteTransaction); // Добавляем обработчик события на клик по таблице для удаления транзакции
