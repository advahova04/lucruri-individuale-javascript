import { getRandomAdvice } from './activity.js'; // Импортируем функцию getRandomAdvice из файла activity.js

/**
 * Обновляет элемент на странице новым текстом активности.
 * @returns {Promise<void>} Обещание, которое разрешается, когда обновление завершено.
 */
async function updateAdvice() {
    const adviceElement = document.getElementById('advice'); // Получаем элемент с идентификатором 'advice'
    const advice = await getRandomAdvice(); // Ожидаем получения случайного совета с помощью функции getRandomAdvice
    adviceElement.textContent = advice; // Устанавливаем текстовое содержимое элемента на странице как полученный совет
}

updateAdvice(); // Вызываем функцию updateAdvice один раз при загрузке страницы
setInterval(updateAdvice, 60000); // Устанавливаем интервал для вызова функции updateAdvice каждые 60000 миллисекунд (1 минута)
