/**
 * Получает случайный совет.
 * @returns {Promise<string>} Обещание, содержащее текст случайного совета.
 */
export async function getRandomAdvice() {
    try {
        const response = await fetch('https://api.adviceslip.com/advice'); // Отправляем запрос к API для получения случайного совета
        const data = await response.json(); // Ожидаем преобразования ответа в формат JSON
        return data.slip.advice; // Возвращаем текст совета из полученных данных
    } catch (error) {
        console.error('Error fetching advice:', error); // Выводим ошибку в консоль, если запрос не удался
        return "К сожалению, произошла ошибка"; // Возвращаем сообщение об ошибке
    }
}
